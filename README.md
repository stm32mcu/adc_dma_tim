# ADC_DMA_TIM

Using ADC triggered by a timer and DMA output to a buffer.

The timer generates 48 kHz sample frequency from the 48 MHz core clock. 

Every time when it rolls over, it generates an update event:

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE

The ADC is configured to make an ADC conversion when that timer update event fires:

    hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_TRGO;

The ADC is instructed to fill a buffer with samples per DMA, without CPU intervention:

    HAL_ADC_Start_DMA(&hadc, (uint32_t*)adc_buf, ADC_BUF_LEN);

The buffer is a circular (ring) buffer, which is filled with the samples.

As soon as the first half of the buffer is filled, a DMA interrupt is generated which eventually calles a callback function:
```
    void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
      processBuffer( &adc_buf[0], ADC_BUF_LEN/2 );
    }
```
The callback function must process the first half of the buffer before the second half is full. 
Analogously, for the second half, there is a another callback:
```
    void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
      processBuffer( &adc_buf[ADC_BUF_LEN/2], ADC_BUF_LEN/2 );
    }
```
When the buffer is large enough, there may be enough time for processBuffer do save/process/output the samples. 

# Notes

The output via USART2 (ST-LINK VCP) is not reliable because of the high throughput, samples will get lost :-(

You may want to add an oversampling features for suppressing alialing. Some STM32 families have hardware support for that (ADC averaging). 
And/or add an analog lowpass (RC filter) to the ADC input.

When porting the code, check the Reference Manual for which TIM, ADC and DMA channels can be combined.

Similar projects: 
* STM AN5012 Application note "Analog-to-digital audio conversion example using STM32L4 Series microcontroller peripherals"